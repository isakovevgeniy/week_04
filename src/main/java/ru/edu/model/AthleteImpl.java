package ru.edu.model;

import java.util.Objects;

public class AthleteImpl implements Athlete {
    private final String lastName;
    private final String firstName;
    private final Country country;
    private final int speed;

    public AthleteImpl(String pLastName, String pFirstName, Country pCountry, int pSpeed) {
        lastName = pLastName;
        firstName = pFirstName;
        country = pCountry;
        speed = pSpeed;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public Country getCountry() {
        return country;
    }

    /**
     * Скорость.
     * @return значение
     */
    public int getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "firstName=" + firstName + '\n' +
                "lastName=" + lastName + '\n' +
                "country=" + country + '\n';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AthleteImpl human = (AthleteImpl) o;
        return Objects.equals(firstName, human.firstName)
                && Objects.equals(lastName, human.lastName)
                && Objects.equals(country, human.country);
    }

    @Override
    public int hashCode() {
        int result = 7 * firstName.hashCode()
                + 17 * lastName.hashCode()
                + 31 * country.hashCode();
        return result;
    }
    
}

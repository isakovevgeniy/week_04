package ru.edu.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CountryParticipantImpl implements CountryParticipant {
    private final Country country;
    private List<Athlete> athletes = new ArrayList<>();
    private long score;

    private List<Participant> participants = new ArrayList<>();

    public CountryParticipantImpl(Country pCountry) {
        country = pCountry;
    }

    public Country getCountry() {
        return country;
    }
    /**
     * Название страны.
     * @return название
     */
    @Override
    public String getName() {
        return country.toString();
    }

    /**
     * Добавление участника в список участников.
     */
    public void addParticipant(Participant participant) {
        participants.add(participant);
    }

    /**
     * Добавление атлетов в список атлетов.
     */
    public void addAthletes(String pLastName, String pFirstName, Country pCountry, int pSpeed) {
        addAthletes(new AthleteImpl(pLastName,pFirstName, pCountry, pSpeed));
    }

    public void addAthletes(Athlete athlete) {
        athletes.add(athlete);
    }


    /**
     * Список участников от страны.
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     * Список атлетов от страны.
     * @return список участников
     */
    public List<Athlete> getAthletes() {
        return athletes;
    }

    /**
     * Счет страны.
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    /**
     * Счет страны.
     */
    public void setScore(long pScore) {
        score = pScore;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n").append(country).append(":\n");
        if (participants.isEmpty()) {
            sb.append("Нет участников").append('\n');
        } else {
            participants.stream()
                    .sorted((p1, p2) -> (int) (p1.getScore() - p2.getScore()))
                    .forEach(p -> sb
                            .append(p.getAthlete().getLastName())
                            .append(" ")
                            .append(p.getAthlete().getFirstName())
                            .append(" ")
                            .append(p.getScore())
                            .append("\n"));
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryParticipantImpl that = (CountryParticipantImpl) o;
        return country == that.country;
    }

    @Override
    public int hashCode() {
        return Objects.hash(country);
    }
}

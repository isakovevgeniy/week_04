package ru.edu.model;

import java.util.Objects;

public class ParticipantImpl implements Participant{
    private long id = 0L;
    private Athlete athlete;
    private long score;

    public ParticipantImpl(long pId, Athlete pAthlete) {
        id = pId;
        athlete = pAthlete;
    }

    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене
     *
     * @return объект спортсмена
     */
    @Override
    public Athlete getAthlete() {
        return athlete;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    public void upDateScore(long pScore) {
        score += pScore;
    }

    public void move() {
        score += (long) athlete.getSpeed() * (Math.random()+1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantImpl that = (ParticipantImpl) o;
        return Objects.equals(athlete, that.athlete);
    }

    @Override
    public int hashCode() {
        return athlete.hashCode();
    }

    @Override
    public String toString() {
        return athlete.getCountry() + " " +
                athlete.getLastName() + " " +
                athlete.getFirstName() + " " +
                score +
                "\n";
    }
}

package ru.edu.model;

import java.util.Locale;

public enum Country {
    RUSSIA,
    USA,
    UKRAINE,
    SPAIN,
    ITALY,
    MEXICO,
    OTHER_COUNTRY;

    public static Country getCountry(String name) {
        String CountryName = name.toLowerCase();
        switch (CountryName) {
            case ("russia"):
                return RUSSIA;
            case ("usa"):
                return USA;
            case ("ukraine"):
                return UKRAINE;
            case ("spain"):
                return SPAIN;
            case ("italy"):
                return ITALY;
            case ("mexico"):
                return MEXICO;
            default:
                return OTHER_COUNTRY;
        }
    }
}

package ru.edu.model;

/**
 * Информация о спортсмене.
 */
public interface Athlete {

    /**
     * Имя.
     *
     * @return значение
     */
    String getFirstName();

    /**
     * Фамилия.
     *
     * @return значение
     */
    String getLastName();

    /**
     * Страна.
     *
     * @return значение
     */
    Country getCountry();

    /**
     * Скорость.
     * @return значение
     */
    public int getSpeed();
}

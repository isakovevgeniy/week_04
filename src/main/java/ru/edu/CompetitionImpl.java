package ru.edu;

import ru.edu.model.*;

import java.util.*;

public class CompetitionImpl implements Competition {
    /**
     * Название состязания.
     */
    private final String name;
    /**
     * Счетчик участников состязания.
     */
    private long participantsCount = 0L;
    /**
     * Коллекция участников.
     */
    private Map<Long, Participant> competitors = new HashMap<>();
    /**
     * Коллекция стран участниц.
     */
    private Map<Country, CountryParticipantImpl> countries = new HashMap<>();

    public CompetitionImpl(String pName) {
        name = pName;
    }

    public String getName() {
        return name;
    }

    public long getParticipantsCount() {
        return participantsCount;
    }

    public Map<Long, Participant> getCompetitors() {
        return competitors;
    }

    public Map<Country, CountryParticipantImpl> getCountries() {
        return countries;
    }

    /**
     * Регистрация участника.
     *
     * @param athlete - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete athlete) {
        Participant participant = new ParticipantImpl(participantsCount, athlete);
        if (competitors.containsValue(participant)) {
            throw new IllegalArgumentException("Участник уже зарегистрирован");
        } else {
            register(participant);
            participantsCount++;
            return participant;
        }
    }

    /**
     * Запись участника в журнал участников.
     * Запись страны участника в журнал стран участниц.
     * Запись участника в список участников в страны участницы
     * @param pParticipant
     */
    private void register(Participant pParticipant) {
        competitors.put(participantsCount, pParticipant);
        Country country = pParticipant.getAthlete().getCountry();
        if (!countries.containsKey(country)) {
            CountryParticipantImpl newCountryParticipant;
            newCountryParticipant = new CountryParticipantImpl(
                    pParticipant.getAthlete().getCountry()
            );
            countries.put(pParticipant.getAthlete().getCountry(), newCountryParticipant);
        }
        countries.get(country).addParticipant(pParticipant);
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        competitors.get(id).upDateScore(score);
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        List<Participant> participants = new ArrayList<>();

        List<Participant> results = new ArrayList<>(competitors.values());
        for (Participant participant : results) {
            long id = participant.getId();
            participants.add(competitors.get(id));
        }
        participants.sort((o1, o2) -> {
            if (o1.getScore() > o2.getScore()) {
                return -1;
            } else if (o1.getScore() < o2.getScore()) {
                return 1;
            } else return 0;
        });
        return participants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        List<Participant> results = new ArrayList<>(competitors.values());
        for (Participant participant : results) {
            writeCountriesResult(participant);
        }
        List<CountryParticipant> countryParticipants_2 = new ArrayList<>(countries.values());
        countryParticipants_2.sort((o1, o2) -> {
            if (o1.getScore() > o2.getScore()) {
                return -1;
            } else if (o1.getScore() < o2.getScore()) {
                return 1;
            } else return 0;
        });
        return countryParticipants_2;
    }

    /**
     * Запись статистики рузультатов состязания по странам.
     * @param participant участник состязаний.
     */
    private void writeCountriesResult(Participant participant) {
        Country country = participant.getAthlete().getCountry();
        Long participantScore = participant.getScore();
        Long countryScore = countries.get(country).getScore();
        countries.get(country).setScore(countryScore + participantScore);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(":\n");
        if (competitors.isEmpty()) {
            sb.append("Нет участников").append('\n');
        } else {
            List<Map.Entry<Long, Participant>> competitorsList;
            competitorsList = new ArrayList<>(competitors.entrySet());
            competitorsList.forEach(s -> sb.append(s.getValue()));
        }
        return sb.toString();
    }

    /**
     * Старт состязания.
     */
    public void run() {
        List<Participant> competitorsList = new ArrayList<>(competitors.values());
        for (Participant participant : competitorsList) {
            for (int i = 0; i < 2; i++) {
                participant.move();
            }
        }
    }
}

package ru.edu;

import ru.edu.model.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SberGames {
    private List<CountryParticipantImpl> countriesCompetitors = new ArrayList<>();
    private List<CompetitionImpl> competitions = new ArrayList<>();
    private Map<Country, Long> gamesResults = new TreeMap<>();

    public List<CountryParticipantImpl> getCountriesCompetitors() {
        return countriesCompetitors;
    }
    public List<CompetitionImpl> getCompetitions() {
        return competitions;
    }

    /**
     * Проведение соревнований.
     * @param args
     */
    public static void main(String[] args) {
        SberGames sberGames2021 = new SberGames();

        sberGames2021.preparing();
        sberGames2021.performing();
        sberGames2021.getResults();
        sberGames2021.printResults();
    }

    /**
     * Подготовка соревнований.
     */
    private void preparing() {
        addCountries();

        String[] copmetitionNames = {
                "swimming",
                "running",
                "jumping",
                "coding"
        };

        addCompetition(copmetitionNames);
        addAthletesFromFile();
        registerAthletes();
    }

    /**
     * Добаввление стран участниц в список стран участниц.
     */
    private void addCountries() {
        register(Arrays.asList(
                Country.RUSSIA,
                Country.USA,
                Country.UKRAINE,
                Country.SPAIN,
                Country.ITALY,
                Country.MEXICO
        ));
    }

    /**
     * Регистрация страны участницы. Запись страны в список стран участниц.
     * @param countries
     */
    public void register(List<Country> countries) {
        for (Country country: countries) {
            register(country);
            System.out.println("Страна " + country.name() + " зарегистрирована");
        }
    }
    /**
     * Регистрация страны участницы.
     * @param country - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    public void register(Country country) {
        if (countriesCompetitors.contains(country)) {
            throw new IllegalArgumentException("Страна - участник уже зарегистрирована");
        } else {
            countriesCompetitors.add(new CountryParticipantImpl(country));
        }
    }

    /**
     * Создание объекта состязание. добавление в журнал.
     * @param competitionNames
     */
    public void addCompetition(String[] competitionNames) {
        for(String name: competitionNames) {
            competitions.add(new CompetitionImpl(name));
        }
    }

    /**
     * Добавление атлетов из файлов стран в списки стран участниц.
     */
    private void addAthletesFromFile() {
        for(CountryParticipantImpl country: countriesCompetitors) {
            String fileName = "./src/main/resources/athletes" + country.getName() + ".dat";
            addAthletes(fileName, country);
            System.out.println("Атлеты из страны "
                    + country.getName()
                    + " прибыли на соревнования");
        }
    }
    /**
     * Чтение данных из файла.
     */
    public void addAthletes(String fileName, CountryParticipantImpl country) {
        try (
                BufferedReader reader = new BufferedReader(new FileReader(fileName))
        ) {
            while (reader.ready()) {
                String line = reader.readLine();
                String[] data = line.split(" ");
                country.addAthletes(data[0]
                        , data[1] + " " + data[2]
                        , country.getCountry()
                        , Integer.parseInt(data[3]));
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Регистрация атлетов.
     */
    private void registerAthletes() {
        for (CompetitionImpl competition: competitions) {
            addAthletesToCompetition(competition);
            System.out.println("Атлеты на участие в соревновании "
                    + competition.getName()
                    + " зарегистрированы ");
        }
    }
    /**
     * Добавление атлетов на состязание.
     * @param competition
     */
    public void addAthletesToCompetition(CompetitionImpl competition) {
        for(CountryParticipantImpl country: countriesCompetitors) {
            for (Athlete athlete : country.getAthletes()) {
                competition.register(athlete);
            }
        }
        System.out.printf("Количество участников составляет %d человек\n"
                , competition.getCompetitors().size());
    }

    /**
     * Ход соревнований.
     */
    private void performing() {
        System.out.println("SberGames 2021 стартовали");
        for (Competition competition: competitions) {
            competition.run();
        }
        System.out.println("SberGames 2021 завершены");
    }
    /**
     * Подсчет результатов по странам.
     */
    public void getResults() {
        System.out.println("Подсчет результатов SberGames 2021");
        for (CompetitionImpl competition: competitions) {
            List<CountryParticipant> countiesEachCompetitionResult
                    = competition.getParticipantsCountries();

            for (CountryParticipant countryParticipant: countiesEachCompetitionResult) {
                Country country = Country.valueOf(countryParticipant.getName());

                if (gamesResults.containsKey(country)) {
                    gamesResults.put(country, gamesResults.get(country)
                            + countryParticipant.getScore());
                } else {
                    gamesResults.put(country, countryParticipant.getScore());
                }
            }
        }
    }

    /**
     * Вывод на экран результатов по странам.
     * Сортировка результатов от большего счета к меньшему.
     */
    public void printResults() {
        System.out.println("Результыты SberGames 2021:");
        List<Map.Entry<Country,Long>> mapEntry
                = new ArrayList<>(gamesResults.entrySet());

        Comparator<Map.Entry<Country,Long>> scoreComparator
                = (o1, o2) -> {
                    if (o1.getValue() > o2.getValue()) {
                        return -1;
                    } else if (o1.getValue() < o2.getValue()) {
                        return 1;
                    } else return 0;
                };

        mapEntry.stream()
                .sorted(scoreComparator)
                .forEach(x -> System.out.printf("%10s %5s\n",x.getKey(),x.getValue()));
    }
}


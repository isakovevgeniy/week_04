package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;

public class AthleteImplTest {
    AthleteImpl athlete = new AthleteImpl("Ivanov", "Ivan", Country.USA, 8);
    AthleteImpl athleteEqualsTest_1 = new AthleteImpl("Ivanov", "Ivan", Country.USA, 8);
    AthleteImpl athleteEqualsTest_2 = new AthleteImpl("Ivanov", "Petr", Country.USA, 8);

    String info ="firstName=" + "Ivan" + '\n' +
                "lastName=" + "Ivanov" + '\n' +
                "country=" + Country.USA + '\n';

    @Test
    public void getFirstNameTest() {
        Assert.assertEquals("Ivan", athlete.getFirstName());
        Assert.assertNotEquals("petr", athlete.getFirstName());
    }

    @Test
    public void getLastNameTest() {
        Assert.assertEquals("Ivanov", athlete.getLastName());
        Assert.assertNotEquals("petr", athlete.getFirstName());
    }

    @Test
    public void getCountryTest() {
        Assert.assertEquals(Country.USA, athlete.getCountry());
        Assert.assertNotEquals(Country.RUSSIA, athlete.getFirstName());
    }

    @Test
    public void getSpeedTest() {
        Assert.assertEquals(8, athlete.getSpeed());
        Assert.assertNotEquals(9, athlete.getSpeed());
    }

    @Test
    public void testToStringTest() {
        Assert.assertEquals(info, athlete.toString());
    }

    @Test
    public void testHashCodeTest() {
        Assert.assertEquals(athlete.hashCode(), athleteEqualsTest_1.hashCode());
        Assert.assertNotEquals(athlete.hashCode(), athleteEqualsTest_2.hashCode());
    }

    @Test
    public void athleteEqualsTest() {
        Assert.assertTrue(athlete.equals(athlete));
        Assert.assertFalse(athlete.equals(null));
        Assert.assertFalse(athlete.equals(new Object()));
        Assert.assertTrue(athlete.equals(athleteEqualsTest_1));
        Assert.assertFalse(athlete.equals(athleteEqualsTest_2));
    }
}
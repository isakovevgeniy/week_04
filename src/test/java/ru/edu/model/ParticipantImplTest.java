package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class ParticipantImplTest {

    AthleteImpl athlete = new AthleteImpl("Ivan", "Ivanov", Country.USA, 8);
    AthleteImpl athleteEqualsTest_1 = new AthleteImpl("Ivan", "Ivanov", Country.USA, 8);
    AthleteImpl athleteEqualsTest_2 = new AthleteImpl("Petr", "Ivanov", Country.USA, 8);

    ParticipantImpl participant = new ParticipantImpl(1L, athlete);
    ParticipantImpl participantEqualsTest = new ParticipantImpl(2L, athleteEqualsTest_1);
    ParticipantImpl participantEqualsTest_2 = new ParticipantImpl(3L, athleteEqualsTest_2);

    @Test
    public void getIdTest() {
        Assert.assertEquals(Long.valueOf(1), participant.getId());
    }

    @Test
    public void getAthleteTest() {
        Assert.assertEquals(athlete, participant.getAthlete());
    }

    @Test
    public void getScoreTest() {
        Assert.assertEquals(0, participant.getScore());
    }
    @Test
    public void upDateScoreTest() {
        participant.upDateScore(10L);
        Assert.assertEquals(10L, participant.getScore());
    }

    @Test
    public void moveTest() {
        long score = participant.getScore();
        participant.move();
        Assert.assertTrue( score < participant.getScore());
    }

    @Test
    public void testEqualsTest() {
        Assert.assertEquals(participant, participantEqualsTest);
    }

    @Test
    public void testHashCodeTest() {
        Assert.assertEquals(participant.hashCode(), participantEqualsTest.hashCode());
        Assert.assertNotEquals(participant.hashCode(), participantEqualsTest_2.hashCode());
    }

    @Test
    public void toStringTest() {
        String sb = athlete.getCountry() + " " +
                athlete.getLastName() + " " +
                athlete.getFirstName() + " " +
                participant.getScore() +
                "\n";
        Assert.assertEquals(sb, participant.toString());
    }

    @Test
    public void participantEqualsTest() {

        Assert.assertTrue(participant.equals(participant));
        Assert.assertFalse(participant.equals(null));
        Assert.assertFalse(participant.equals(athlete));
        Assert.assertTrue(participant.equals(participantEqualsTest));
        Assert.assertFalse(participant.equals(participantEqualsTest_2));
    }
    @Test
    public void participantHashCodeTest() {
        Assert.assertEquals(participant.hashCode(), participantEqualsTest.hashCode());
        Assert.assertNotEquals(participant.hashCode(), participantEqualsTest_2.hashCode());
    }
}

package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;

public class CountryTest {

    @Test
    public void getCountryTest(){
        Assert.assertEquals("RUSSIA", Country.getCountry("RUSSIA").toString());
        Assert.assertEquals("USA", Country.getCountry("USA").toString());
        Assert.assertEquals("MEXICO", Country.getCountry("MEXICO").toString());
        Assert.assertEquals("ITALY", Country.getCountry("ITALY").toString());
        Assert.assertEquals("SPAIN", Country.getCountry("SPAIN").toString());
        Assert.assertEquals("UKRAINE", Country.getCountry("UKRAINE").toString());
        Assert.assertEquals("OTHER_COUNTRY", Country.getCountry("UZBEKISTAN").toString());
    }
}

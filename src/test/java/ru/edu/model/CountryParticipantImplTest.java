package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;

public class CountryParticipantImplTest {
    CountryParticipantImpl countryParticipant = new CountryParticipantImpl(Country.MEXICO);
    AthleteImpl athlete = new AthleteImpl("Ivan", "Ivanov", Country.USA, 8);
    ParticipantImpl participant = new ParticipantImpl(1L, athlete);

    @Test
    public void getNameTest() {
        Assert.assertEquals("MEXICO", countryParticipant.getName());
    }

    @Test
    public void addParticipantTest() {
        int participantsListSize = countryParticipant.getParticipants().size();
        countryParticipant.addParticipant(participant);
        Assert.assertEquals(participantsListSize + 1, countryParticipant.getParticipants().size());
    }

    @Test
    public void addAthletesTest_1() {
        int alhtetesListSize = countryParticipant.getAthletes().size();
        countryParticipant.addAthletes("Sidorov", "Sidor", Country.RUSSIA, 10);
        Assert.assertEquals(alhtetesListSize + 1, countryParticipant.getAthletes().size());
    }
    @Test
    public void addAthletesTest_2() {
        int alhtetesListSize = countryParticipant.getAthletes().size();
        countryParticipant.addAthletes(athlete);
        Assert.assertEquals(alhtetesListSize + 1, countryParticipant.getAthletes().size());
    }

    @Test
    public void getParticipantsTest() {
        Assert.assertNotNull(countryParticipant.getParticipants());
        addParticipantTest();
        Assert.assertTrue(countryParticipant.getParticipants().get(0) instanceof ParticipantImpl);
    }

    @Test
    public void getAthletesTest() {
        Assert.assertNotNull(countryParticipant.getParticipants());
        addAthletesTest_1();
        Assert.assertNotNull(countryParticipant.getAthletes().get(0));
    }

    @Test
    public void getScoreTest() {
        Assert.assertEquals(0L, countryParticipant.getScore());
    }

    @Test
    public void testToStringTest() {
        String emptyInfo = "\n" + countryParticipant.getName() + ":\n" + "Нет участников\n";
        Assert.assertEquals(emptyInfo, countryParticipant.toString());

        addParticipantTest();
        String fillInfo =  "\n" + countryParticipant.getName() + ":\n" +
                countryParticipant.getParticipants().get(0).getAthlete().getLastName() +
                " " +
                countryParticipant.getParticipants().get(0).getAthlete().getFirstName() +
                " " +
                countryParticipant.getParticipants().get(0).getScore() +
                "\n";
        Assert.assertEquals(fillInfo, countryParticipant.toString());

    }
    @Test
    public void countryImplEqualsTest() {
        Assert.assertTrue(countryParticipant.equals(countryParticipant));
        Assert.assertFalse(countryParticipant.equals(null));
        Assert.assertFalse(countryParticipant.equals(participant));
        Assert.assertTrue(countryParticipant.equals(new CountryParticipantImpl(Country.MEXICO)));
        Assert.assertFalse(countryParticipant.equals(new CountryParticipantImpl(Country.USA)));
    }
}
package ru.edu;

import org.junit.Assert;
import org.junit.Test;
import ru.edu.model.Country;
import ru.edu.model.CountryParticipantImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SberGamesTest {
    SberGames testGames = new SberGames();
    CountryParticipantImpl countryParticipant = new CountryParticipantImpl(Country.MEXICO);

    @Test
    public void registerTest() {
        int countriesCount = testGames.getCountriesCompetitors().size();
        testGames.register(Country.UKRAINE);
        Assert.assertEquals(countriesCount +1, testGames.getCountriesCompetitors().size());
        Assert.assertEquals(Country.UKRAINE.toString(), testGames.getCountriesCompetitors().get(0).getName());
    }

    @Test
    public void testRegisterTest() {
        int countriesCount = testGames.getCountriesCompetitors().size();
        List<Country> countries = Arrays.asList(Country.RUSSIA,Country.USA);
        testGames.register(countries);
        Assert.assertEquals(countriesCount +2, testGames.getCountriesCompetitors().size());
        Assert.assertEquals(Country.RUSSIA.toString(), testGames.getCountriesCompetitors().get(0).getName());
        Assert.assertEquals(Country.USA.toString(), testGames.getCountriesCompetitors().get(1).getName());
    }

    @Test
    public void addCompetitionTest() {
        int competitionsCount = testGames.getCompetitions().size();
        testGames.addCompetition(new String[]{"running"});
        testGames.addCompetition(new String[] {"jumping"});
        Assert.assertEquals(competitionsCount +2, testGames.getCompetitions().size());
        Assert.assertEquals("running", testGames.getCompetitions().get(0).getName());
        Assert.assertEquals("jumping", testGames.getCompetitions().get(1).getName());
    }

    @Test
    public void getCompetitionsTest() {
        Assert.assertTrue(testGames.getCompetitions() instanceof ArrayList);
        testGames.addCompetition(new String[]{"running"});
        Assert.assertFalse(testGames.getCompetitions().isEmpty());
    }

    @Test
    public void getResultsTest() {

    }

    @Test
    public void printResultsTest() {
    }

    @Test
    public void addAthletesToCompetitionTest() {
    }

    @Test
    public void addAthletesTest() {
        int alhtetesListSize = countryParticipant.getAthletes().size();
        testGames.addAthletes("./src/test/java/ru/edu/model/athletesRUSSIATest.dat", countryParticipant);
        Assert.assertEquals(alhtetesListSize + 3, countryParticipant.getAthletes().size());
    }

    @Test(expected = RuntimeException.class)
    public void addAthletesExceptionTest() {
        testGames.addAthletes("./src/test/java/ru/edu/model/Test.dat", countryParticipant);
    }

    @Test
    public void mainTest() {
    }
}
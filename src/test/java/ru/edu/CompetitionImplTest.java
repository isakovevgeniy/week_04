package ru.edu;

import org.junit.Assert;
import org.junit.Test;
import ru.edu.model.*;

import java.util.List;

public class CompetitionImplTest {
    CompetitionImpl competition = new CompetitionImpl("coding");
    AthleteImpl athlete = new AthleteImpl("Ivanov", "Ivan", Country.USA, 8);
    AthleteImpl athlete_2 = new AthleteImpl("Ivanov_2", "Ivan", Country.RUSSIA, 8);
    AthleteImpl athlete_3 = new AthleteImpl("Ivanov_3", "Ivan", Country.SPAIN, 8);
    AthleteImpl athlete_4 = new AthleteImpl("Ivanov_4", "Ivan", Country.RUSSIA, 8);

    ParticipantImpl participant = new ParticipantImpl(0L, athlete);

    @Test
    public void registerTest() {
        Assert.assertEquals(participant, competition.register(athlete));
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerTrowExceptionTest() {
        competition.register(athlete);
        competition.register(athlete);
    }

    @Test
    public void register_2_Test() {
        long participantsCount = competition.getParticipantsCount();
        int countriesCount = competition.getCountries().size();
        int competitorsCount = competition.getCompetitors().size();

        Participant USA_Participant = competition.register(athlete);
        Participant Russia_Participant_2 = competition.register(athlete_2);
        Participant Russia_Participant_4 = competition.register(athlete_4);

        Assert.assertEquals(participantsCount + 3L, competition.getParticipantsCount());
        Assert.assertEquals(countriesCount + 2, competition.getCountries().size());
        Assert.assertEquals(competitorsCount + 3, competition.getCompetitors().size());

        Assert.assertEquals(participant, competition.getCompetitors().get(0L));
        Assert.assertEquals(USA_Participant, competition.getCompetitors().get(0L));
        Assert.assertEquals(Russia_Participant_2, competition.getCompetitors().get(1L));
        Assert.assertEquals(Russia_Participant_4, competition.getCompetitors().get(2L));

        Assert.assertTrue(competition.getCountries().get(Country.USA).getParticipants().contains(USA_Participant));
        Assert.assertTrue(competition.getCountries().get(Country.RUSSIA).getParticipants().contains(Russia_Participant_2));
        Assert.assertTrue(competition.getCountries().get(Country.RUSSIA).getParticipants().contains(Russia_Participant_4));
    }

    @Test
    public void updateScoreTest() {
        competition.register(athlete);
        competition.updateScore(0L, 10L);
        Assert.assertEquals(10L, competition.getCompetitors().get(0L).getScore());
    }

    @Test
    public void testUpdateScoreTest() {
        competition.register(athlete);
        competition.updateScore(participant, 10L);
        Assert.assertEquals(10L, competition.getCompetitors().get(0L).getScore());
    }

    @Test
    public void getResultsTest() {
        Assert.assertTrue(competition.getResults().isEmpty());
        competition.register(athlete);
        competition.register(athlete_2);
        competition.register(athlete_3);

        competition.updateScore(0, 10L);
        competition.updateScore(1, 3L);
        competition.updateScore(2, 60L);

        List<Participant> results =  competition.getResults();

        Assert.assertEquals(3L, results.get(2).getScore());
        Assert.assertEquals(10L, results.get(1).getScore());
        Assert.assertEquals(60L, results.get(0).getScore());
    }

    @Test
    public void getParticipantsCountriesTest() {
        Assert.assertTrue(competition.getParticipantsCountries().isEmpty());

        competition.register(athlete);
        competition.register(athlete_2);
        competition.register(athlete_3);
        competition.register(athlete_4);

        competition.updateScore(0, 10L);    //usa
        competition.updateScore(1, 4L);     //russia
        competition.updateScore(2, 60L);    //spain
        competition.updateScore(3, 8L);    //russia

        List<CountryParticipant> results =  competition.getParticipantsCountries();

        Assert.assertEquals("SPAIN", results.get(0).getName());     // 1 место 60 очков
        Assert.assertEquals("RUSSIA", results.get(1).getName());    // 2 место 12 очков
        Assert.assertEquals("USA", results.get(2).getName());       // 3 место 10 очков
    }

    @Test
    public void testToStringTest() {
        String sb = competition.getName() + ":\n";
        String sbEmpty = sb + "Нет участников" + '\n';
        String sbNotEmpty = sb
                            + athlete.getCountry() + ' '
                            + athlete.getLastName() + ' '
                            + athlete.getFirstName() + ' '
                            + participant.getScore()
                            + '\n';

        Assert.assertEquals(sbEmpty, competition.toString());
        competition.register(athlete);
        Assert.assertEquals(sbNotEmpty, competition.toString());
    }

    @Test
    public void runTest() {
        competition.register(athlete);
        long scoreBeforeStart = participant.getScore();
        competition.run();
        Assert.assertTrue(scoreBeforeStart < competition.getCompetitors().get(0L).getScore());
    }
}
